//limpia el canvas
function clearCanvas() {
  context.clearRect(0, 0, background.width, background.height);
}

/**Genera nuevas instancias de la clase vibora cada cierto numero de frames, determinado por el 
 * @param level y las guarda en un array y genera posiciones en Y de manera aleatoria*/
function generateSerpent(level) {
  const posY = Math.floor(Math.random() * (canvas.height - 100));
  if (frames % (100 - level * 9) === 0) {
    viboras.push(new Viboras(posY));
  }
}

//Llama al método de draw para mostrar todas las instancias de la clase vibora
function drawSerpents() {
  viboras.forEach((vibora) => vibora.draw());
}

//Establece la animación de las instancias de la clase vibora cada 5 frames
function serpentAnimation() {
  viboras.forEach((vibora) => {
    if (frames % 5 === 0) {
      if (vibora.animate === 7) {
        vibora.animate = 0;
      } else {
        vibora.animate++;
      }
    }
  });
}

/**Detecta cuando una instancia de la clase vibora sale de un rango del canvas y  
 * las elimina del array para liberar espacio*/
function deleteSerpent() {
  viboras.forEach((vibora, i) => {
    if (vibora.x + vibora.width > canvas.width - 80) {
      viboras.splice(i, 1);
    }
  });
}

/**Genera nuevas instancias de la clase ave cada cierto numero de frames, determinado por el 
 * @param level 
 * y las guarda en un array con posX y posY aleatorios para dibujarse de manera random en el canvas*/
function generateBirds(level) {
  const posX = Math.floor(Math.random() * (canvas.width - 250 - 250) + 250);
  const posY = Math.floor(Math.random() * (canvas.height - 100 - 100) + 100);
  if (frames % (200 - level * 10) === 0) {
    pajaros.push(new Aves(posX, posY));
  }
}

//Llama al método draw para dibujar sobre el canvas las isntancias de la clase ave
function drawBirds() {
  pajaros.forEach((pajaro) => pajaro.draw());
}

//Establece que cada 5 frames, dibuje la animacion de las instancias de la clase ave
function birdsAnimation() {
  pajaros.forEach((pajaro) => {
    if (frames % 5 === 0) {
      if (pajaro.animate === 7) {
        pajaro.animate = 0;
      } else {
        pajaro.animate++;
      }
    }
  });
}

/**Detecta cuando alguna instancia de la clase ave salga del canvas y lo elimina del 
 * array para liberar espacio*/ 
function deleteBird() {
  pajaros.forEach((pajaro, i) => {
    if (pajaro.y > canvas.height) {
      pajaros.splice(i, 1);
    }
  });
}

/**La funcion de Winner verfica que la variable de twoPlayers sea true para realizar la comparacion de 
 * score de cada isntancia de jugador y verificar quien gano. En caso de que se trate de un solo jugador solo 
 * devuelve el valor de su score, con un mensaje en pantalla*/
function winner() {
  cancionInicio.pause();
  winnerSong.play();
  if (twoPlayers === true) {
    if (gatito1.score > gatito2.score) {
      context.font = "55px cursive";
      context.fillStyle = "red";
      context.fillText(`GANÓ ${gatito1.nombreJugador}`, 180, 200);
      context.font = "38px cursive";
      context.fillStyle = "white";
      context.fillText(`Puntuación: ${gatito1.score}`, 285, 260);
    } else if (gatito2.score > gatito1.score) {
      context.font = "55px cursive";
      context.fillStyle = "red";
      context.fillText(`GANÓ ${gatito1.nombreJugador}`, 180, 200);
      context.font = "38px cursive";
      context.fillStyle = "white";
      context.fillText(`Puntuación: ${gatito1.score}`, 285, 260);
    } else if (gatito2.score === gatito1.score) {
      context.font = "55px cursive";
      context.fillStyle = "red";
      context.fillText("HA HABIDO UN EMPATE", 180, 200);
    }
  } else {
    context.font = "55px cursive";
    context.fillStyle = "red";
    context.fillText("BIEN JUGADO!", 180, 200);
    context.font = "38px cursive";
    context.fillStyle = "white";
    context.fillText(`Puntuación: ${gatito1.score}`, 285, 260);
  }
  context.font = "28px cursive";
  context.fillStyle = "yellow";
  context.fillText(`Presione I para volver a inicio`, 215, 340);
}


/**Verifica si la vriable twoPlayers es true y compara si las vidas de algun jugador es 0, establece al ganador 
 * a quien aun tenga vidas. En caso de un solo jugador, solo devulve l puntuacion y temina el juego */
function gameOver() {
  cancionInicio.pause();
  gameOverSong.play();
  if (twoPlayers === true) {
    if (gatito2.vidas === 0) {
      context.font = "55px cursive";
      context.fillStyle = "red";
      context.fillText(`GANÓ ${gatito1.nombreJugador}`, 180, 200);
      context.font = "38px cursive";
      context.fillStyle = "white";
      context.fillText(`Puntuación: ${gatito1.score}`, 285, 260);
    } else if (gatito1.vidas === 0) {
      context.font = "55px cursive";
      context.fillStyle = "red";
      context.fillText(`GANÓ ${gatito2.nombreJugador}`, 180, 200);
      context.font = "38px cursive";
      context.fillStyle = "white";
      context.fillText(`Puntuación: ${gatito2.score}`, 285, 260);
    }
  } else {
    context.font = "55px cursive";
    context.fillStyle = "red";
    context.fillText("FIN DEL JUEGO!", 180, 200);
    context.font = "38px cursive";
    context.fillStyle = "white";
    context.fillText(`Puntuación: ${gatito1.score}`, 285, 260);
  }
  context.font = "28px cursive";
  context.fillStyle = "yellow";
  context.fillText(`Presione I para volver a inicio`, 215, 340);
  cancionInicio.loop = false;
}

/**Pinta sobre el canvas el nivel en el que se  encuentra el jugador, cada nivel aumenta con la siguiente formula
 * cada 50 frames aumenta la variable time en uno y cada time % 10  aumenta el nivel, simulando un tiempo de
 * 10 segundos para aumentar el nivel, de igual forma envia como argumento el nivel para generar serpientes y aves*/
function drawLevel() {
  if (frames % 50 == 0) {
    time++;
  }
  if (time % 10) {
    nivel = Math.floor(time / 10);
    generateSerpent(nivel);
    generateBirds(nivel);
  }

  context.font = "25px cursive";
  context.fillStyle = "white";
  context.fillText(`Nivel: ${nivel}`, canvas.width - 200, canvas.height - 20);
}

//dibuja sobre el canvas el score de cada jugador si twoPlayers es true, o solo la de un jugador en caso contrario
function drawScore() {
  context.font = "25px cursive";
  context.fillStyle = "white";
  context.fillText(
    `Puntuación P1: ${gatito1.score}`,
    canvas.width - 200,
    canvas.height - 50
  );
  if (twoPlayers === true) {
    context.font = "25px cursive";
    context.fillStyle = "white";
    context.fillText(
      `Puntuación P2: ${gatito2.score}`,
      canvas.width - 200,
      canvas.height - 80
    );
  }
}
