/**
 *La clase {@code Background}, establece las propiedades y métodos para dibujar un background infinito que 
 *ocupa todo el canvas*/
class Background {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.width = canvas.width;
    this.height = canvas.height;
    this.img = new Image();
    this.img.src = imagenes.escenario;
    this.img.onload = () => {
      this.draw();
    };
  }
  draw() {
    this.y++;
    if (this.y > this.height) this.y = 0;
    context.drawImage(this.img, this.x, this.y, this.width, this.height);
    context.drawImage(
      this.img,
      this.x,
      this.y - this.height,
      this.width,
      this.height
    );
  }
}

/** {@code Gatito} esta clase contiene un constructor que recibe la posicion en x donde se ubicara en el canvas,
la imagen del jugador y el nombre del jugador*/
class Gatito {
  constructor(x, imagenJugador, nombreJugador) {
    this.width = 100;
    this.height = 100;
    this.x = x;
    this.y = canvas.height - this.height - 50;
    this.vx = 0;
    this.vy = 0;
    this.vidas = 5;
    this.score = 0;
    this.animate = 0; // anima el movimiento
    this.position = 2; // establece el movimiento que se desea
    this.nombreJugador = nombreJugador;
    this.img = new Image();
    this.img.src = imagenJugador;
    this.vidasImg = new Image();
    this.vidasImg.src = imagenes.imgVidas;
    this.img.onload = () => {
      this.draw();
    };
  }
  /*Dibuja a la instancia en determinada posición y estableciendo limites dentro del canvas
  y dibuja el nombre del jugador sobre la imagen*/
  draw() {
    if (this.x < 200) {
      this.x = 200;
    } else if (this.x + this.width + 200 > canvas.width) {
      this.x = canvas.width - this.width - 200;
    } else if (this.y < 0) {
      this.y = 0;
    } else if (this.y + this.height > canvas.height) {
      this.y = canvas.height - this.height;
    }
    context.drawImage(
      this.img,
      (this.animate * 640) / 4,
      (this.position * 1280) / 8,
      640 / 4,
      1280 / 8,
      this.x,
      this.y,
      this.width,
      this.height
    );
    context.font = "10px cursive";
    context.fillStyle = "white";
    context.fillText(`${this.nombreJugador} `, this.x + 25, this.y + 30);
  }
  //desplaza al jugador hacia la izquierda
  moveLeft() {
    this.vx -= 3;
    this.position = 3;
  }
  //desplaza al jugador hacia la derecha
  moveRight() {
    this.vx += 3;
    this.position = 1;
  }
  //desplaza al jugador hacia arriba
  moveTop() {
    this.vy -= 3;
    this.position = 2;
  }
  //desplaza al jugador hacia abajo
  moveDown() {
    this.vy += 3;
    this.position = 0;
  }

  //Establece que cada 5 frames cambie la animacion sobre una posición
  catAnimation() {
    if (frames % 5 === 0) {
      if (this.animate === 3) {
        this.animate = 0;
      } else {
        this.animate++;
      }
    }
  }

  //Variables de velocidad para el desplazamiento en x,y
  updateXY() {
    this.x += this.vx;
    this.y += this.vy;
  }

  /**Detecta las colisiones con cada instancia de las clases vibora y ave. Cada vez que exista alguna colisión
   * y las elimina del array donde son almacenadas, de igual forma, en caso de colisionar con una vibora
   * disminuye la vida del jugador y establece un sonido. En caso de colisionar con un ave, aumenta el score
   * y establece un sonido*/

  collisions() {
    viboras.forEach((vibora, i) => {
      /**@return{boolean}  true si es existe colision*/
      if (
        this.x + 60 <= vibora.x + vibora.width &&
        this.x + this.width - 35 >= vibora.x &&
        this.y + 35 <= vibora.y + vibora.height &&
        this.y + this.height - 60 >= vibora.y
      ) {
        viboras.splice(i, 1);
        this.vidas--;
        ataqueGatito.play();
      }
    });
    pajaros.forEach((pajaro, i) => {
      if (
        this.x + 18 <= pajaro.x + pajaro.width &&
        this.x + this.width - 18 >= pajaro.x &&
        this.y + 30 <= pajaro.y + pajaro.height &&
        this.y + this.height >= pajaro.y
      ) {
        pajaros.splice(i, 1);
        this.score++;
        gatitoVida.play();
      }
    });
  }

  /**Muestra sobre el canvas las vidas con las que cuenta cada jugador y checa los metodos de winner en caso de 
   * completar los 10 niveles y gameOver por si algun jugador pierde sus vidas antes de completar los niveles*/
  showLives(posY) {
    context.font = "16px cursive";
    context.fillStyle = "white";
    context.fillText(`${this.nombreJugador} `, 0, posY + 18);

    let x = 95;
    let y = posY;
    for (let i = 0; i < this.vidas; i++) {
      context.drawImage(this.vidasImg, x, y, 25, 25);
      x += 30;
    }
    if (nivel < 10) {
      if (this.vidas === 0) {
        clearInterval(interval);
        gameOver();
      }
    } else {
      clearInterval(interval);
      winner();
    }
  }

  //establece la animación por defecto que tendré el jugador
  update() {
    this.position = 2;
    this.vx = 0;
    this.vy = 0;
  }
}

/**{@code Viboras} contiene las propiedades y metodos de la clase vibora. Tiene un constructor que recibe 
 * @param  Y ,posición en Y
 *la cual permite dibuajrlo de manera aleatoria sobre el canvas y con direccion a la derecha */
class Viboras {
  constructor(y) {
    this.width = 100;
    this.height = 100;
    this.x = 80;
    this.y = y;
    this.animate = 0; // anima el movimiento
    this.position = 1; // establece el movimiento que se desea hacer
    this.img = new Image();
    this.img.src = imagenes.vibora;
    this.img.onload = () => {
      this.draw();
    };
  }

  //Dibuja la animacion que debe tener la isntancia de la clase
  draw() {
    this.x += 3;
    context.drawImage(
      this.img,
      (this.animate * 256) / 8,
      (this.position * 160) / 5,
      256 / 8,
      160 / 5,
      this.x,
      this.y,
      this.width,
      this.height
    );
  }
}

/**La clase {@code Aves} tiene un constructor que recibe los valores de 
  *@param  x
  *@param  y
  *para pintar de manera aleatoria sobre el canvas*/
class Aves {
  constructor(x, y) {
    this.width = 35;
    this.height = 35;
    this.x = x;
    this.y = y;
    this.animate = 0; // anima el movimiento
    this.position = 0; // establece el movimiento que se desea hacer
    this.img = new Image();
    this.img.src = imagenes.aves;
    this.img.onload = () => {
      this.draw();
    };
  }

  //Establece la animacion y pinta sobre el canvas la instancia
  draw() {
    this.y++;
    context.drawImage(
      this.img,
      (this.animate * 473) / 8,
      (this.position * 296) / 5,
      473 / 8,
      296 / 5,
      this.x,
      this.y,
      this.width,
      this.height
    );
  }
}
