/**
 * @fileoverview Proyecto 1- Camino a casa
 * 
 * Version 1.0
 * @author Jazmin Martínez Hernández
 * Bootcamp Kavak
 */

 // Variables que contienen elementos del documento
const container = document.querySelector("#container");
const canvas = document.querySelector("#canvas");
const context = canvas.getContext("2d");
const audio = document.querySelector("audio");

//Array de imagenes
const imagenes = {
  escenario: "./images/background.jpg",
  jugador: "./images/gatitos.png",
  jugador2: "./images/gatos.png",
  vibora: "./images/viboras.png",
  aves: "./images/aves.png",
  imgVidas: "./images/vidas.png",
  gameOver: "./images/gameOver.png",
};

//Variables globales
let frames = 0;
let interval;
let viboras = [];
let pajaros = [];
let twoPlayers = false;
let gatito2;

let nivel = 0;
let time = 0;
let vx = 0;
audio.volume = 0.05;

//función que inicia el juego una vez presionado el modo del jugador
function startGame() {
  if (interval) return;
  audio.pause();
  cancionInicio.play();
  cancionInicio.volume = 0.2;
  cancionInicio.loop = true;
  interval = setInterval(update, 1000 / 60);
}

//función que actualiza el juego y llama a todos los métodos de las clases
function update() {
  clearCanvas();
  frames++;
  background.draw();
  drawLevel();

  /**verifica que el modo de dos jugadores exista para poder llamar a los metodos de el segundo jugador
   * asi como los a las funciones que pintan a las demas clases sobre el canvas y establecen cada operacion*/
  if (twoPlayers === true) { 
    gatito2.catAnimation();
    gatito2.draw();
    gatito2.updateXY();
    gatito2.collisions();
    gatito2.showLives(40);
  }

  gatito1.catAnimation();
  gatito1.draw();
  gatito1.updateXY();
  gatito1.collisions();
  gatito1.showLives(10);

  drawSerpents();
  serpentAnimation();
  deleteSerpent();

  drawBirds();
  birdsAnimation();
  deleteBird();

  drawScore();
}