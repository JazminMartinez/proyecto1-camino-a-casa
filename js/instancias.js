//Crea las instancias del jugador, el background y los audios que incluye el proyecto
const background = new Background();
const gatito1 = new Gatito(300, imagenes.jugador, "JUGADOR 1");

let ataqueGatito = new Audio("sounds/sonidoGato.mp3");
let gatitoVida = new Audio("sounds/gatitoVida.mp3");
let cancionInicio = new Audio("sounds/mainSong.mp3");
let gameOverSong = new Audio("sounds/gameOver.mp3");
let winnerSong = new Audio("sounds/winner.mp3");
