window.onload = function () {
  //Cuando el boton de unJugador es presionado comienza el modo un jugador
  document.getElementById("unJugador").onclick = function () {
    container.style.display = "none";
    canvas.style.display = "block";
    startGame();
  };

 /**Cuando da click sobre el boton de dos jugadores, se crea la isntancia del jugador 2 y el 
  * juego comienza en modo de dos jugadores*/
  document.getElementById("dosJugadores").onclick = function () {
    container.style.display = "none";
    canvas.style.display = "block";
    gatito2 = new Gatito(400,imagenes.jugador2,'JUGADOR 2');
    twoPlayers = true;
    startGame();
  };

  //Muestra las instrucciones de los controles de los jugadores, al dar click sobre le boton de instrucciones
  document.getElementById("instrucciones").onclick = function () {
    container.style.display = "none";
    mostrarInstrucciones.style.display = "inline-block";
  };

  //Devuelve al inicio del juego, cuando da click sobre el botón de inicio
  document.getElementById("inicio").onclick = function () {
    container.style.display = "flex";
    mostrarInstrucciones.style.display = "none";
  };

  /**Detecta cuando la tecla correspondiente a los controles de los jugadores, es presionada y 
   * llama al metodo de la clase gatito que le corresponde*/
  document.addEventListener("keydown", ({ keyCode }) => {
    if(twoPlayers){
      switch (keyCode) {
        case 65:
          gatito2.moveLeft();
          break;
        case 68:
          gatito2.moveRight();
          break;
        case 87:
          gatito2.moveTop();
          break;
        case 83:
          gatito2.moveDown();
          break;
      }
    }
    switch (keyCode) {
      case 37:
        gatito1.moveLeft();
        break;
      case 39:
        gatito1.moveRight();
        break;
      case 38:
        gatito1.moveTop();
        break;
      case 40:
        gatito1.moveDown();
        break;
      //En caso de presionar la tecla I lo devuelve al inicio del juego
      case 73:
        container.style.display = "none";
        canvas.style.display = "block";
        location.reload();
        break;
    }
  });
  
  //actualiza a la instacia de la clase Gatito en la posición establecida por defecto para uno y dos jugadores
  document.addEventListener("keyup", (event) => {
    if (twoPlayers === true) {
      gatito2.update();
    }
    gatito1.update();
  });
};

