#Proyecto 1 - *Camino a casa*

##Autora: Jazmín Martínez Hernández

##Introducción.
Este proyecto esta inspirado en mi aprecio por los gatos, quienes resultan salir de casa y encontrarse con diverso animales, a quienes suelen atacar o bien enfrentarse a ellos y perder.
Por tal motivo existen tres principales personajes que aparecen en el juego:
*Gato
*Serpiente
*Ave


![Imagen Demo](https://gitlab.com/JazminMartinez/proyecto1-camino-a-casa/-/blob/master/images/ImagenDemo.PNG "Imagen Demo")


##Lista de pendientes.
1. Permitir que el usuario reinicie el juego, cuando este termine.
2. Mejorar el proyecto para posible implementación de multijugador, sin tener que repetir código

##Créditos.
1. Imagenes de internet, que no recuerdo sus direcciones.
2. [https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API]
3. Proyectos realizados durante el bootcamp por Eduardo Ch. Colorado



